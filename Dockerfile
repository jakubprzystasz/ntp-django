FROM python:3.9.6-alpine as builder

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh postgresql-dev gcc python3-dev musl-dev jpeg-dev zlib-dev libjpeg 

# set work directory
WORKDIR /usr/src/app/
COPY ./src/requirements.txt ./
# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip
RUN pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels -r requirements.txt

FROM python:3.9.6-alpine as prod

WORKDIR /app/
COPY --from=builder /usr/src/app/wheels /wheels
COPY --from=builder /usr/src/app/requirements.txt .
RUN pip install --no-cache /wheels/*

COPY ./src/ ./